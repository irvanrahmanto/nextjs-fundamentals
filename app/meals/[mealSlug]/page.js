const MealsDetailPage = ({params}) => {
    return(
        <main>
            <h1>This is meals detail pages!</h1>
            <p>{params.slug}</p>
        </main>
    );
}

export default MealsDetailPage;