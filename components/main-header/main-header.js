import Link from "next/link";
import React from "react";
import Image from "next/image";

/** import an image */
import logoImg from "@/assets/logo.png";
import classes from "../../components/main-header/main-header.module.css";
import MainHeaderBackground from "../main-header/main-header-background";
import NavLink from "./nav-link";

const MainHeader = () => {

  return (
    <>
      <MainHeaderBackground />
      <header className={classes.header}>
        <Link className={classes.logo} href="/">
          <Image
            src={logoImg.src}
            width={500}
            height={500}
            alt="A Plate of it"
          />
          NextLevel Food
        </Link>

        <nav className={classes.nav}>
          <ul>
            <li>
              <NavLink href="/meals">Browse Meals</NavLink>
            </li>
            <li>
              <NavLink href="/community">Foodies Community</NavLink>
            </li>
          </ul>
        </nav>
      </header>
    </>
  );
};

export default MainHeader;
